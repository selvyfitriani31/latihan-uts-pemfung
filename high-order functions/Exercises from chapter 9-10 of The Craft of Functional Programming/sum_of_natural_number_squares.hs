sumSquare :: Int -> Int
sumSquare n = foldr (+) 0 $ map (^2) [1..n]