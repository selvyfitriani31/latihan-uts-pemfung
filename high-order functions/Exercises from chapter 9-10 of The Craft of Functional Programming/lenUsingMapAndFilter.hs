myLength :: [a] -> Int
myLength xs = sum (map (\x -> 1) xs)
