triplePhyta = [(x,y,z)
                | z <- [5..], 
                  y <- [z,z-1..1],
                  x <- [y,y-1..1],
                  x^2 + y^2 == z^2  
                ]   