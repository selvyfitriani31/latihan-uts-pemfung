quicksort [] = []
quicksort [x] = [x]
quicksort (x:xs) = quicksort([y | y<-xs , y<x]) ++ [x] ++ quicksort([z | z<-xs , z>=x])