import Data.List 
perms [] = [[]]
perms ls = [(x:ps) | x <- ls, ps <- perms(ls\\[x])]